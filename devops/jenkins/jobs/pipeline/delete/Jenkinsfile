pipeline
{
  agent 
  {
    label 'jenkins-ecs-slave'
  }
  options 
  {
    skipDefaultCheckout true 
  }
  environment 
  {
    CLOUDFORMATION_STAGE = true
  }
  stages 
  {
    stage('Checkout SCM') 
    {
      steps 
      {
        script 
        {
          checkout scm
        }
      }
    }
    stage('Update Slack') 
    {
      steps 
      {
        script 
        {
          //== Slack parameters
          SLACK="devops/jenkins/scripts/Utilities/sendToSlack.sh 'xoxp-50850716951-200824430182-292471917479-7a2a1d676be42e690e53e7326dcc9ae9' '#jenkins-ci-dev'"
          
          sh("${SLACK} '*STARTED:* Job _${JOB_NAME} [${BUILD_NUMBER}]_ (${BUILD_URL})'")
        }
      }
    }
    stage('Deploy Parameters') 
    {
      steps 
      {
        script 
        {
          // Flow Variables
          IS_FIRST_EXECUTION = (params.INVOKE_DYNAMIC_PARAMETERS == null) ? true : false
          
          // Define dynamic choice parameters
          STACKS = sh(script: "devops/jenkins/scripts/Parameter/getStacksByDescription.sh 'Sets up ecs cluster'", returnStdout: true).trim()
  
          STATIC_PARAMETERS = 
          [
            booleanParam(name: 'INVOKE_DYNAMIC_PARAMETERS', description: 'Invoke Dynamic Parameters', defaultValue: false)
          ]
          
          DYNAMIC_PARAMETERS = 
          [
            choice(name: 'STACK', description: 'Stack', choices: STACKS)
          ]
          
          STATIC_PARAMETERS += DYNAMIC_PARAMETERS
          properties([parameters(STATIC_PARAMETERS)]) 
          
          if (params.INVOKE_DYNAMIC_PARAMETERS == true || IS_FIRST_EXECUTION == true)
          { 
            sh("${SLACK} '*Attention!* Job _${JOB_NAME} [${BUILD_NUMBER}]_ (${BUILD_URL}) is paused and need input parameters!'")
            STACK = input(message: 'Choose your variables!', ok: 'Set', parameters: DYNAMIC_PARAMETERS)
          }
        }
      }
    }
    stage('Validate Resources')
    {
      steps 
      {
        script
        {
          //== Validade parameters
          if (!STACKS.contains(STACK))
            error("Invalid STACK!")
        }
      } 
    }
    stage('Define variables') 
    {
      steps 
      {
        script 
        {
          //== Cloudformation parameters
          REGION = STACK.tokenize(' : ')[0]
          STACK_NAME = STACK.tokenize(' : ')[1]
          CLUSTER_NAME = STACK.tokenize(' : ')[1].split('-')[0]
          
          //== Others parameters
          TYPE_API_GATEWAY = sh(script: "devops/jenkins/scripts/Utilities/getStackParameter.sh ${REGION} ${STACK_NAME} TypeApiGateway", returnStdout: true).trim()
          LAMBDA_API_GATEWAY_PROXY_NAME  = sh(script: "devops/jenkins/scripts/Utilities/getStackParameter.sh ${REGION} ${STACK_NAME} LambdaApiGatewayProxyName", returnStdout: true).trim()
        }
      }
    }
    stage ('Delete Cluster Lambda-Functions')
    {
      parallel
      {
        stage ('Lambda-ECSEventsCheck')
        {
          steps
          {
            build job: 'lambda-ECSEventsCheck-delete', propagate: false, parameters:
            [
              booleanParam(name: 'INVOKE_DYNAMIC_PARAMETERS', value: false),
              string(name: 'STACK', value: "${REGION} : lambda-ECSEventsCheck-${CLUSTER_NAME}")
            ]
          }
        }
        stage ('Lambda-AlarmToSlack')
        {
          steps
          {
            build job: 'lambda-AlarmToSlack-delete', propagate: false, parameters:
            [
              booleanParam(name: 'INVOKE_DYNAMIC_PARAMETERS', value: false),
              string(name: 'STACK', value: "${REGION} : lambda-AlarmToSlack-${CLUSTER_NAME}")
            ]
          }
        }
        stage ('Lambda-ApiGatewayAuthorizer')
        {
          steps
          {
            build job: 'lambda-ApiGatewayAuthorizer-delete', propagate: false, parameters:
            [
              booleanParam(name: 'INVOKE_DYNAMIC_PARAMETERS', value: false),
              string(name: 'STACK', value: "${REGION} : lambda-ApiGatewayAuthorizer-${CLUSTER_NAME}")
            ]
          }
        }
        stage ('Lambda-ApiGatewayProxy')
        {
          steps
          {
            build job: 'lambda-ApiGatewayProxy-delete', propagate: false, parameters:
            [
              booleanParam(name: 'INVOKE_DYNAMIC_PARAMETERS', value: false),
              string(name: 'STACK', value: "${REGION} : lambda-ApiGatewayProxy-${CLUSTER_NAME}")
            ]
          }
        }
      }
    }
    stage('Remove ApiGatewayProxy Association')
    {
      when { expression { TYPE_API_GATEWAY == "AWS_PROXY" } }
      steps
      {
        build job: 'lambda-ApiGatewayProxy-deploy', propagate: false, parameters:
        [
          booleanParam(name: 'INVOKE_DYNAMIC_PARAMETERS', value: false),
          string(name: 'REGION', value: params.REGION),
          string(name: 'NAME', value: LAMBDA_API_GATEWAY_PROXY_NAME)
        ]
      }
    }
    stage('Delete Cloudformation')
    {
      steps 
      {
        script
        {
          CLOUDFORMATION_STAGE = true
          sh("aws cloudformation delete-stack --region ${REGION} --stack-name ${STACK_NAME}")
          sh("aws cloudformation wait stack-delete-complete --region ${REGION} --stack-name ${STACK_NAME}")
        }
      }  
    }
  }
  post 
  {
    always 
    {
      script 
      {
        RESULT=(currentBuild.result == null ? "SUCCESSFUL" : currentBuild.result)
        
        if (CLOUDFORMATION_STAGE == true)
          sh("aws cloudformation describe-stack-events --stack-name ${STACK_NAME} --region ${REGION} | jq '.StackEvents[] | .LogicalResourceId + \" - \" + .ResourceStatus + \" - \" + .ResourceStatusReason'")
          
        sh("${SLACK} '*${RESULT}:* Job _${JOB_NAME} [${BUILD_NUMBER}]_ (${BUILD_URL})'")
      }
    }
  }
}
