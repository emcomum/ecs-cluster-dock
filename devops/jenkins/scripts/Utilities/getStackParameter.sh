#!/bin/bash

#=== Script Inputs
REGION=$1
STACK_NAME=$2
PARAMETER=$3
#=================

aws cloudformation describe-stacks --region $REGION | jq -r '.[] | .[] | select (.StackName == "'$STACK_NAME'") | (.Parameters[] | select(.ParameterKey == "'$PARAMETER'") | .ParameterValue)'