#!/bin/bash

#=== Script Inputs
REGION=$1
STACK_NAME=$2
#================= 

aws cloudformation describe-stacks --region $REGION | jq -r '[.[] | .[] | select (.StackName == "'$STACK_NAME'" and .StackStatus != "DELETE_COMPLETE") ] | if length > 0 then  "YES" else "NO" end'

# devops/jenkins/scripts/Utilities/stackExists.sh us-east-1 "weslei26-cluster"