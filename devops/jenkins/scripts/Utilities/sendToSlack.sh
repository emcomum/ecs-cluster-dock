#!/bin/bash

#=== Script Inputs
TOKEN=$1
CHANNEL=$2
MESSAGE=$3
#=================

curl  -X POST https://slack.com/api/chat.postMessage \
      -H "Content-Type: application/json;  charset=utf-8" \
      -H "Authorization: Bearer $TOKEN" \
      -H "Accept: application/json;  charset=utf-8" \
      -d "{
        'text': '$MESSAGE',
        'channel': '$CHANNEL',
        'username': 'Jenkins',
        'icon_url': 'https://a.slack-edge.com/205a/img/services/jenkins-ci_72.png'
      }"