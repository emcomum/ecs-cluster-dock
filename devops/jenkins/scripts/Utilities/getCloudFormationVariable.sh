#!/bin/bash

#=== Script Inputs
REGION=$1
NAME=$2
#================= 

aws cloudformation list-exports --region $REGION | jq -r '.[] | .[] | select(.Name == "'$NAME'") | .Value' 