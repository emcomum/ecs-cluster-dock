#!/bin/bash

function urlencode() 
{
  local LANG=C i c e=''
  for ((i=0;i<${#1};i++))
  do
    c=${1:$i:1}
    [[ "$c" =~ [a-zA-Z0-9\.\~\_\-] ]] || printf -v c '%%%02X' "'$c"
    e+="$c"
  done
  echo "$e"
}

#=== Script Inputs
GIT_URL=$1
GIT_BRANCH=$2
#================= 

REQUEST=`aws secretsmanager get-secret-value --region us-east-1 --secret-id Jenkins`
GIT_EMAIL=`echo "$REQUEST" | jq -r '.SecretString' | jq -r '.GITHUB_EMAIL'`
GIT_NAME=`echo "$REQUEST" | jq -r '.SecretString' | jq -r '.GITHUB_NAME'`
GIT_USERNAME=`echo "$REQUEST" | jq -r '.SecretString' | jq -r '.GITHUB_USER'`
GIT_PASSWORD=`echo "$REQUEST" | jq -r '.SecretString' | jq -r '.GITHUB_PASSWORD'`

git add -A
git -c "user.name=$GIT_NAME" -c "user.email=$GIT_EMAIL" commit -m 'Save Configuration [ci skip]'
git push "${GIT_URL//https\:\/\//https\:\/\/`urlencode $GIT_USERNAME`\:`urlencode $GIT_PASSWORD`\@}" HEAD:$GIT_BRANCH