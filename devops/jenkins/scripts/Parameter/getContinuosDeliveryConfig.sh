#!/bin/bash

#=== Script Inputs
BRANCH=$1
#================= 

if [ -d "devops/config" ]
then
  for file in `find devops/config -maxdepth 1 -type f -iname "*-$BRANCH.yml"`
  do
    while IFS='' read -r line || [[ -n "$line" ]]
    do
      IFS=': ' read -r -a array <<< "$line"
      if [ "${array[0]}" == "CONTINUOUS_DELIVERY" ]
      then
        if [ "${array[1]}" == "true" ]
        then
          cat "$file"
        fi
      fi
    done < "$file"
  done
fi