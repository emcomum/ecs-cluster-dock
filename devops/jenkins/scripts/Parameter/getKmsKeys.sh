#!/bin/bash

ACCOUNT_ID=`aws sts get-caller-identity --output text --query 'Account'`

for REGION in `aws ec2 describe-regions --region=us-east-1 --output text --query 'Regions[*].RegionName'`
do
  RESULT=$RESULT`aws kms list-aliases --region $REGION | jq -r '.[] | .[] | select(.TargetKeyId != null) | "'$REGION' : " + .AliasName + " (arn:aws:kms:'$REGION':'$ACCOUNT_ID':key/" + .TargetKeyId + ")"'`"\n"
done

echo -e "WITHOUT\n$RESULT" | grep .