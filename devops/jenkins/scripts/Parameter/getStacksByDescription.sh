#!/bin/bash

#=== Script Inputs
DESCRIPTION=$1
#================= 

for REGION in `aws ec2 describe-regions --region=us-east-1 --output text --query 'Regions[*].RegionName'`
do
  RESULT=$RESULT`aws cloudformation describe-stacks --region $REGION | jq -r '.[] | .[] | select (.Description == "'"$DESCRIPTION"'" and .RootId == null) | "'$REGION' : " + .StackName'`"\n"
done

echo -e "$RESULT" | grep .