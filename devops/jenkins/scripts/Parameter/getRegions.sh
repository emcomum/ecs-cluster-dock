#!/bin/bash

aws ec2 describe-regions --region=us-east-1 | jq -r '.[] | .[] | .RegionName'