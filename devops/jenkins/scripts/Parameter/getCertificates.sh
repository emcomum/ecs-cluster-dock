#!/bin/bash

for REGION in `aws ec2 describe-regions --region=us-east-1 --output text --query 'Regions[*].RegionName'`
do
  RESULT=$RESULT`aws acm list-certificates --region $REGION | jq -r '.CertificateSummaryList | .[] | "'$REGION' : " + .DomainName + " (" + .CertificateArn + ")"'`"\n"
done

echo -e "$RESULT" | grep .