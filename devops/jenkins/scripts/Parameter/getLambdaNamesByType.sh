#!/bin/bash

#=== Script Inputs
TYPE=$1
#================= 

for REGION in `aws ec2 describe-regions --region=us-east-1 --output text --query 'Regions[*].RegionName'`
do
  RESULT=$RESULT`aws cloudformation describe-stacks --region $REGION | jq -r '[.[] | .[] | [select (.Description == "Sets up ecs cluster" and .RootId == null) | .Parameters[] | select(.ParameterKey == "ClusterName") | "'$REGION' : '$TYPE'-" + .ParameterValue] + [select (.Description == "Sets up '$TYPE' lambda") | "'$REGION' : " + (.Parameters[] | select(.ParameterKey == "Name") | .ParameterValue)] | .[]] | group_by(.) | .[] | select(length == 1) | .[]'`"\n"
done

echo -e "CREATE_NEW\n$RESULT" | grep .