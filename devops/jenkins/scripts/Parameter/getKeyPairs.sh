#!/bin/bash

for REGION in `aws ec2 describe-regions --region=us-east-1 --output text --query 'Regions[*].RegionName'`
do
  RESULT=$RESULT`aws ec2 describe-key-pairs --region $REGION | jq -r  '.[] | .[] | "'$REGION' : " + .KeyName'`"\n"
done

echo -e "$RESULT" | grep .